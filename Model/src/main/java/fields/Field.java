package fields;

import java.util.ArrayList;

import security.SecurityGroup;

public class Field {
	int id;
	String name;
	String fieldSource; // JSON path
	ArrayList<SecurityGroup> securityGroups;
	FieldType fieldType;
	DataType dataType;
	Boolean controlled;
	
	
	public enum FieldType {
		SYSTEM,
		USER
	}
	
	public enum DataType {
		DATE,
		NUMBER,
		TEXT
	}
}
