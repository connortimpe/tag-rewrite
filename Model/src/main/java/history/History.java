package history;

import java.util.ArrayList;

import historyDetail.HistoryDetail;
import historyMetadata.HistoryMetadata;

public class History {
	
	int id;
	int userId;
	long historyEpoch;
	HistoryMetadata historyMetadata;
	ArrayList<HistoryDetail> historyDetail;
	
	HistoryType historyType;
	
	public enum HistoryType {
		TAG,
		RELATIVE,
		DOCUMENTATTRIBUTE,
		FAMILY
	}
	
	
}
