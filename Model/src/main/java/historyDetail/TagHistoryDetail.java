package historyDetail;

import java.util.ArrayList;

import tags.Tag;

public class TagHistoryDetail {

	ArrayList<Integer> activeTagIds;
	ArrayList<TagHistoryDetailMetadata> tagHistoryDetailMetadata;
	
	public enum CascadeType {
		FAMILY,
		RELATIVES,
		DUPLICATES
	}
	
	public enum ActionTaken {
		ADDED,
		REMOVED
	}
	
	public void addTagHistoryDetailMetadata(int tagId, int ruleId, CascadeType cascadeType, ActionTaken actionTaken) {
		TagHistoryDetailMetadata tagHistoryDetail = new TagHistoryDetailMetadata();
		
		tagHistoryDetail.setTagId(tagId);
		tagHistoryDetail.setRuleId(ruleId);
		tagHistoryDetail.setCascadeType(cascadeType);
		tagHistoryDetail.setActionTaken(actionTaken);
		
		if(tagHistoryDetailMetadata == null) {
			tagHistoryDetailMetadata = new ArrayList<TagHistoryDetailMetadata>();
		}
		
		tagHistoryDetailMetadata.add(tagHistoryDetail);
	}
	
	
	class TagHistoryDetailMetadata {
		int tagId;
		int ruleId;
		CascadeType cascadeType;
		ActionTaken actionTaken;
		
		public int getTagId() {
			return tagId;
		}
		public void setTagId(int tagId) {
			this.tagId = tagId;
		}
		public int getRuleId() {
			return ruleId;
		}
		public void setRuleId(int ruleId) {
			this.ruleId = ruleId;
		}
		public CascadeType getCascadeType() {
			return cascadeType;
		}
		public void setCascadeType(CascadeType cascadeType) {
			this.cascadeType = cascadeType;
		}
		public ActionTaken getActionTaken() {
			return actionTaken;
		}
		public void setActionTaken(ActionTaken actionTaken) {
			this.actionTaken = actionTaken;
		}
		
		
	}
	
}
