package rules;

import java.util.ArrayList;

import tags.Tag;

public class TagRule {
//Types of tag rules:
//	Must select another tag
//	Can not select another tag
//  Automatically adds another tag
//  Automatically removes another tag
	
	
	int id;
	String name; 
	ArrayList<TagRuleBaseTag> baseTags;
	ArrayList<TagRuleTargetTag> targetTags; 
	BaseTagOperator baseTagOperator;
	
	public enum BaseTagOperator {
		ANYOF,
		ALLOF
	}
	
	
	RuleAction action;
	public enum RuleAction {
		REQUIRE_ADD,
		FORBID_ADD,
		AUTO_ADD,
		AUTO_REMOVE
	}
	
	
	public boolean applyRule() {
		//handle applying auto add or auto remove rules
		
		return true; //return if rule is applied successfully
	}
	
	public boolean verifyRules() {
		//make sure rules are applied - ex: require add or forbid add rules
		return true; //return if rules are satisfied
	}
	
	
}
