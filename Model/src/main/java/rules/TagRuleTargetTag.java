package rules;

public class TagRuleTargetTag {
	int id;
	TagRuleScope tagRuleScope;
	TagRuleAction tagRuleAction;
	
	public enum TagRuleScope {
		TAG,
		TAGGROUP
	}
	
	public enum TagRuleAction {
		ADD,
		REMOVE,
		ENFORCESELECT
	}
}
