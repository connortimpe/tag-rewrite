package security;

import java.util.ArrayList;

import misc.User;

public class SecurityGroup {
	int id;
	int firmId;
	String name;
	ArrayList<User> users; //users attached to the security group
	
//	permissions
	boolean canView; // see and select values -- cannot change the actual tags
	boolean canEdit; // alter the tags
	boolean canDelete;
	boolean isReadOnly;
	
	
	public boolean canView() {
		return canView;
	}
	
	public boolean has(User user) {
		return users.contains(user);
	}
	
	
	//If user is in more than one security group, which value should we use? Most permissive / most restrictive?
	
}

//delete: tag from system (deactivate)    