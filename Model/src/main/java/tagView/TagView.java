package tagView;

import java.util.ArrayList;

import misc.User;
import rules.TagRule;
import security.SecurityGroup;

import javax.persistence.*;

@Entity
@Table(name="tagView")
public class TagView { //Also called a palette
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	int id;
	@Column
	String name;
	@Column
	User viewOwner;
	@Column
	ViewType viewType;
	ArrayList<TagViewGroup> tagViewGroups;
	ArrayList<TagRule> tagRules;
	ArrayList<SecurityGroup> securityGroups;

	//add - add(), delete() for lists
	
	public enum ViewType {
		USER,
		SYSTEM
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getViewOwner() {
		return viewOwner;
	}

	public void setViewOwner(User viewOwner) {
		this.viewOwner = viewOwner;
	}

	public ViewType getViewType() {
		return viewType;
	}

	public void setViewType(ViewType viewType) {
		this.viewType = viewType;
	}

	public ArrayList<TagViewGroup> getTagViewGroups() {
		return tagViewGroups;
	}

	public void setTagViewGroups(ArrayList<TagViewGroup> tagViewGroups) {
		this.tagViewGroups = tagViewGroups;
	}

	public ArrayList<TagRule> getTagRules() {
		return tagRules;
	}

	public void setTagRules(ArrayList<TagRule> tagRules) {
		this.tagRules = tagRules;
	}

	public ArrayList<SecurityGroup> getSecurityGroups() {
		return securityGroups;
	}

	public void setSecurityGroups(ArrayList<SecurityGroup> securityGroups) {
		this.securityGroups = securityGroups;
	}

	public boolean canView(User user) {
		for(SecurityGroup securityGroup : securityGroups) {
			if(securityGroup.canView() && securityGroup.has(user)) {
				return true;
			}
		}
		return false;
	}
	
	
}