package tagView;

import fields.Field;
import tags.TagGroup;

public class TagViewGroup {
	Boolean editable;
	String headerBackgroundColor;
	TagViewGroupType tagViewGroupType;
	TagGroup tagGroup;
	Field field;
	
	public enum TagViewGroupType {
		TAGGROUP,
		FIELD
	}
}
