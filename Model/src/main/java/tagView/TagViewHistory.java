package tagView;

public class TagViewHistory {
	int historyId;
	
	int viewId;
	
	int changedTagId;
	
	TransactionType transactionType;
	
	public enum TransactionType {
		CURRENT_TAG,
		INSERT_TAG_NORMAL,
		REMOVE_TAG_NORMAL,
		INSERT_TAG_CASCADE,
		REMOVE_TAG_CASCADE,
		INSERT_TAG_TRIGGER_CASCADE,
		REMOVE_TAG_TRIGGER_CASCADE,
	}
	
	//need to record new and old values?
	
}
