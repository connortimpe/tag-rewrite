package tags;

import java.util.List;

import rules.TagRule;
import security.SecurityGroup;

import javax.persistence.*;

@Entity
public class Tag {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ids", nullable = false)
	private Long ids;

	int id;
	String value;
	String defaultLetterColor; //Only used outside of a view (ex: flex)
	List<SecurityGroup> securityGroups; 
	Boolean privileged;
	
	//Cascade Preferences:
	boolean cascadeExactDuplicates;
	boolean cascadeFamily;
	boolean cascadeRelatives;

	public Long getIds() {
		return ids;
	}

	public void setIds(Long ids) {
		this.ids = ids;
	}

	public boolean cascadeToExactDuplicates() {
		return cascadeExactDuplicates;
	}
	
	public boolean cascadeToFamily() {
		return cascadeFamily;
	}
	
	public boolean cascadeToRelatives() {
		return cascadeRelatives;
	}
}
