package tags;

import java.util.List;

import rules.TagRule;
import security.SecurityGroup;

public class TagGroup {
	int id;
	String name;
	Layout layout;
	List<TagViewTag> tags;
	List<SecurityGroup> securityGroups; // or some other security
	Type type;
	
    
    public enum Layout {
    	SINGLE_COLUMN, //  |
    	DOUBLE_COLUMN, // | |
    	TRIPLE_COLUMN  // |||
    }
    
    public enum Type {
    	RADIO,
    	SELECT,
    	CHECKBOX
    }
}
