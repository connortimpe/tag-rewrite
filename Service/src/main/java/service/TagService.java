package service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import tagView.TagView;
import tags.Tag;
import tags.TagGroup;

//@EnableAutoConfiguration
//@EnableJpaRepositories("TagRewrite")
//@ComponentScan(basePackages = { "TagRewrite" })
//@EntityScan("TagRewrite")
//@SpringBootApplication(scanBasePackages={"TagRewrite"})
//@Import(TagView.class)
@Import({Tag.class, TagGroup.class})
@SpringBootApplication
public class TagService {
    public static void main(String[] args) {
        SpringApplication.run(TagService.class, args);
    }
}