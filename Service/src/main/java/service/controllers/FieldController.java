package service.controllers;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FieldController {


    //Field Endpoints
    @GetMapping(value = "/field")
    String getField() {
        return "field get endpoint hit";
    }

    @PostMapping(value = "/field")
    String postField() {
        return "field post endpoint hit";
    }

    @PutMapping(value = "/field")
    String putField() {
        return "field put endpoint hit";
    }

    @DeleteMapping(value = "/field")
    String deleteField() {
        return "field delete endpoint hit";
    }


}