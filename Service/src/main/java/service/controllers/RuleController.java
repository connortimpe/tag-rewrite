package service.controllers;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RuleController {

    //Rule Endpoints
    @GetMapping(value="/rule")
    String getRule() {
        return "rule get endpoint hit";
    }

    @PostMapping(value="/rule")
    String postRule() {
        return "rule post endpoint hit";
    }

    @PutMapping(value="/rule")
    String putRule() {
        return "rule put endpoint hit";
    }

    @DeleteMapping(value="/rule")
    String deleteRule() {
        return "rule delete endpoint hit";
    }

    //Misc Endpoints
    @PostMapping(value="/applyRules/id")
    String applyRules(@RequestParam("id") int id) {
        return "applyRules post endpoint hit";
    }

    @GetMapping(value="/verifyRules/id")
    String verifyRules(@RequestParam("id") int id) {
        return "verifyRules get endpoint hit";
    }


}
