package service.controllers;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecurityGroupController {

    //Security Group Endpoints
    @GetMapping(value = "/securityGroup")
    String getSecurityGroup() {
        return "securityGroup get endpoint hit";
    }

    @PostMapping(value = "/securityGroup")
    String postSecurityGroup() {
        return "securityGroup post endpoint hit";
    }

    @PutMapping(value = "/securityGroup")
    String putSecurityGroup() {
        return "securityGroup put endpoint hit";
    }

    @DeleteMapping(value = "/securityGroup")
    String deleteSecurityGroup() {
        return "securityGroup delete endpoint hit";
    }


}