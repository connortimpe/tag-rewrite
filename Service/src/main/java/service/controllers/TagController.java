package service.controllers;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TagController {


    //Tag Endpoints
    @GetMapping(value = "/tag")
    String getTag() {
        return "tag get endpoint hit";
    }

    @PostMapping(value = "/tag")
    String postTag() {
        return "tag post endpoint hit";
    }

    @PutMapping(value = "/tag")
    String putTag() {
        return "tag put endpoint hit";
    }

    @DeleteMapping(value = "/tag")
    String deleteTag() {
        return "tag delete endpoint hit";
    }

}