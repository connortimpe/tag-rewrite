package service.controllers;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TagGroupController {

    //Tag Group Endpoints
    @GetMapping(value = "/group")
    String getGroup() {
        return "group get endpoint hit";
    }

    @PostMapping(value = "/group")
    String postGroup() {
        return "group post endpoint hit";
    }

    @PutMapping(value = "/group")
    String putGroup() {
        return "group put endpoint hit";
    }

    @DeleteMapping(value = "/group")
    String deleteGroup() {
        return "group delete endpoint hit";
    }

}