//package service.controllers;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//import service.services.ViewService;
//import tagView.TagView;
//
//@RestController
//public class ViewController {
//    @Autowired
//    ViewService viewService;
//
//    //View Endpoints
//    @GetMapping(value = "/view")
//     TagView getView(@RequestParam("id") int viewId) {
//        System.out.println("id: " + viewId);
//        return viewService.getViewById(viewId);
////        return "view get endpoint hit";
//    }
//
//    @GetMapping(value = "/test")
//    String test() {
//        return "test received";
//    }
//
//    @PostMapping(value = "/view")
//    private int postView(@RequestBody TagView view) {
//        viewService.saveOrUpdate(view);
//        return view.getId();
////        return "view post endpoint hit";
//    }
//
//    @PutMapping(value = "/view")
//    String putView(@RequestParam("id") int viewId) {
//        return "view put endpoint hit";
//    }
//
//    @DeleteMapping(value = "/view")
//    String deleteView(@RequestParam("id") int viewId) {
//        return "view delete endpoint hit";
//    }
//
//    //Get all views for a user
//    @GetMapping(value = "/view/user")
//    String getAllViews(@RequestParam("id") int userId) {
//        return "view/user get endpoint hit";
//    }
//}