//package service.services;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import service.repositories.ViewRepository;
//import tagView.TagView;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Service
//public class ViewService {
//    @Autowired
//    ViewRepository viewRepository;
//
//    public List<TagView> getAllViews() {
//        List<TagView> views = new ArrayList<TagView>();
//        viewRepository.findAll().forEach(views::add);
//        return views;
//    }
//
//    public TagView getViewById(int id) {
//        return viewRepository.findById(id).get();
//    }
//
//    public void saveOrUpdate(TagView view) {
//        viewRepository.save(view);
//    }
//
//    public void delete(int id) {
//        viewRepository.deleteById(id);
//    }
//
//}
