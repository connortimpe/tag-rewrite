DROP TABLE IF EXISTS tags;

CREATE TABLE tags (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  val VARCHAR(250) NOT NULL,
  letter_color VARCHAR(250) NOT NULL
);

INSERT INTO tags (val, letter_color) VALUES
  ('Responsive', 'blue');


DROP TABLE IF EXISTS tagView;

CREATE TABLE tagView (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  viewOwner VARCHAR(250) NOT NULL,
  viewType VARCHAR(250) NOT NULL
);

INSERT INTO tagView (name, viewOwner, viewType) VALUES
  ('View 1', 'connor', 'user');


