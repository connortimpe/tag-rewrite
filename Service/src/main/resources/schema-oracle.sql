DROP TABLE IF EXISTS tags;

CREATE TABLE tags (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  val VARCHAR(250) NOT NULL,
  letter_color VARCHAR(250) NOT NULL
);

INSERT INTO tags (val, letter_color) VALUES
  ('Responsive', 'blue');
